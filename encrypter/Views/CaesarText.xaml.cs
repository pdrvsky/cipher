﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using encrypter.Cipher;

namespace encrypter.Views
{
    /// <summary>
    ///     Interaction logic for CaesarText.xaml
    /// </summary>
    public partial class CaesarText : Page
    {
        private readonly ICipher _cipher;

        public CaesarText()
        {
            this.InitializeComponent();
            this._cipher = new CaesarCipher(3);
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!this.IsLoaded) return;
            var box = sender as TextBox;

            if (this.Mode.IsChecked == false)
            {
                var encoded = this._cipher.Encrypt(Encoding.Default.GetBytes(box.Text));
                this.Output.Text = Encoding.Default.GetString(encoded);
            }
            else
            {
                var encoded = this._cipher.Decrypt(Encoding.Default.GetBytes(box.Text));
                this.Output.Text = Encoding.Default.GetString(encoded);
            }
        }

        private void Mode_Click(object sender, RoutedEventArgs e)
        {
            if ((this.Mode.IsChecked == true) && (this.Output.Text.Length > 0))
            {
                this.Input.Text = this.Output.Text;
                this.TextBox_TextChanged(this.Input, null);
            }
            else
            {
                this.TextBox_TextChanged(this.Input, null);
            }
        }
    }
}