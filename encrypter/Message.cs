﻿using System;
using System.Net;

namespace encrypter
{
    [Serializable]
    public struct Message
    {
        public IPAddress Sender { get; set; }
        public DateTime Received { get; set; }
        public MessageType Type { get; set; }
        public byte[] Data { get; set; }
    }

    [Serializable]
    public enum MessageType
    {
        SelfText,
        ReceivedText,
        Text,
        Image,
        Other
    }
}