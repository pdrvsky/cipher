﻿using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using encrypter.Cipher;
using encrypter.Views;

namespace encrypter
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ProgramMode _mode = ProgramMode.CaesarText;

        public MainWindow()
        {
            this.InitializeComponent();
            this.Loaded += delegate { this.PageSwitch(); };

            //RSACipher rsa = new RSACipher();
            //rsa.GenerateRSAKeys();

            //AESExtensions.MixColumns(AESExtensions.State(new byte[] { 0xdb, 0x13, 0x53, 0x45, 0x01, 0x05, 0x06, 0x07, 0x01, 0x09, 0x0A, 0x0B, 0x01, 0x0D, 0x0E, 0x10 }));
            //AESExtensions.Rotate( new byte[] { 0xFF, 0x99, 0x66, 0x33 } );

            //var aes = new AESCipher();
            //var testV = new byte[]
            //    {0x6b, 0xc1, 0xbe, 0xe2, 0x2e, 0x40, 0x9f, 0x96, 0xe9, 0x3d, 0x7e, 0x11, 0x73, 0x93, 0x17, 0x2a};
            //var source =
            //    Encoding.Unicode.GetBytes(
            //        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tincidunt tellus massa, sit amet rhoncus tellus consequat eu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent viverra dictum sollicitudin. Integer sit amet enim elementum, tincidunt lorem ac, finibus nibh. Mauris vel justo rhoncus, condimentum purus eget, porta nisi. Donec viverra massa et accumsan fringilla. Donec tristique egestas neque. Morbi pulvinar pellentesque dapibus. Pellentesque lobortis tristique nisi, mattis sagittis enim placerat a. Mauris odio felis, sollicitudin non auctor scelerisque, convallis in lectus. Nam scelerisque turpis nec massa dictum, vitae facilisis purus tincidunt.");
            //var cipher = aes.Encrypt(source);
            //var plain = aes.Decrypt(cipher);

            //Console.WriteLine(Encoding.Unicode.GetString(plain.Take(plain.Length - 2).ToArray()));

            //using (var rsa = new RSACipher())
            //{
            //    var topSecret = "TOP SECRET!";
            //    byte[] secretKey;
            //    byte[] cipher;

            //    string decrypted;

            //    Console.WriteLine("\nTOP SECRET TEXT: " + topSecret + "\n");

            //    using (var aes = new AESCipher())
            //    {
            //        secretKey = rsa.Encrypt(aes.Key);
            //        cipher = aes.Encrypt(Encoding.Default.GetBytes(topSecret));
            //    }

            //    Console.WriteLine("ENCRYPTED!");

            //    using (var aes = new AESCipher(rsa.Decrypt(secretKey)))
            //    {
            //        decrypted = Encoding.Default.GetString(aes.Decrypt(cipher));
            //    }

            //    Console.WriteLine("DECRYPTED!");

            //    Console.WriteLine("\nDECRYPTED TEXT: " + decrypted + "\n");
            //}


            //Connection.Connection cnt = new Connection.Connection();
            //cnt.Connect(System.Net.IPAddress.Parse("127.0.0.1"));
            //cnt.Connected += () => { cnt.Send(Encoding.Default.GetBytes("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis ligula condimentum, lacinia arcu vitae, sollicitudin felis. Fusce at elit eu ante commodo lacinia vel a augue. Ut ut dolor quis leo tincidunt tristique ut in metus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean lacinia nec erat ut varius. Vestibulum cursus velit tortor, a tincidunt justo tincidunt eu. Sed ut tincidunt metus. Etiam vulputate velit eget diam sagittis commodo. Quisque feugiat, sem semper condimentum consequat, lectus nisl pharetra orci, nec elementum sem lorem eu mauris. Praesent in placerat nunc, ac efficitur lectus. Phasellus ac eros lacinia, sodales mauris non, interdum nulla. Morbi vitae fermentum orci. Donec nunc erat, imperdiet ut aliquet non, volutpat quis dolor. Vestibulum tellus dui, bibendum ut placerat convallis, blandit sed turpis. Sed in ipsum consequat, sagittis tellus ut, ullamcorper sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean id pharetra quam, vitae iaculis elit. Nulla facilisi. Ut in risus porta, convallis turpis non, mattis ipsum. Cras sollicitudin enim imperdiet nibh blandit, vel tristique velit ultrices. Sed ac ante ut mi vestibulum dignissim. Suspendisse malesuada feugiat lorem, a porta ante sollicitudin sit amet. Aenean aliquam, dolor et dignissim facilisis, metus eros imperdiet arcu, ac bibendum ipsum magna ut sem. Integer at euismod sem. Vestibulum ante ipsum primis in faucibus orci luctus sed.")); };
            //cnt.MessageReceived += msg => { Console.WriteLine(msg); };
        }



        private void PageSwitch()
        {
            if (!this.IsLoaded) return;

            switch (this._mode)
            {
                case ProgramMode.CaesarText:
                    this.Container.Content = new CaesarText();
                    break;
                case ProgramMode.CaesarImage:
                    this.Container.Content = new CaesarImage();
                    break;
                default:
                    break;
            }
        }

        private void ModeSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this._mode = (ProgramMode) e.AddedItems[0];
            this.PageSwitch();
        }
    }
}