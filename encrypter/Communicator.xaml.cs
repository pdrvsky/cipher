﻿using System;
using System.ComponentModel;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Input;
using encrypter.Connection;

namespace encrypter
{
    /// <summary>
    ///     Interaction logic for Communicator.xaml
    /// </summary>
    public partial class Communicator
    {
        private Protocol _prot;

        public Communicator()
        {
            this.InitializeComponent();
            //this._connection = new Connection();
            //this._connection.MessageReceived += this.Msg;

            this._prot = new Protocol();
            this._prot.DataReceived += this.Msg;
            this._prot.Connected += this.Connected;
            this._prot.Disconnected += this.Disconnected;
        }

        private void Disconnected()
        {
            this.Dispatcher.Invoke(() =>
            {
                this.ConnectionStatus.Content = "Not connected";
            });
        }

        private void Connected(bool isClient)
        {
            this.Dispatcher.Invoke(() => {
                this.ConnectionStatus.Content = "Connected to: " + this._prot.ConnectedIP;
            });
        }

        private void Msg(byte[] msg)
        {
            this.Dispatcher.Invoke(
                () =>
                {
                    var item = new Message()
                    {
                        Data = msg,
                        Received = DateTime.Now,
                        Type = MessageType.ReceivedText
                    };

                    this.MessageList.Items.Add(item);
                    this.ScrollViewer.ScrollToEnd();
                }
            );
        }

        private void ConnectClick(object sender, RoutedEventArgs e)
        {
            var connect = new Connect();
            var showDialog = connect.ShowDialog();
            if ((showDialog == null) || !showDialog.Value) return;

            this._prot.Connect(connect.IP);

            //this._connection.Connect(connect.IP);
            //this._connection.Connected += () => { this.Dispatcher.Invoke(() =>
            //{
            //    this.Title = "Communicator | Connected to: " + connect.IP;
            //}); };
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if ((e.Key != Key.Enter) || !this._prot.IsConnected) return;

            this._prot.Send(Encoding.Default.GetBytes(this.TextBox.Text));
            this.MessageList.Items.Add(
                new Message() { Data = Encoding.Default.GetBytes(this.TextBox.Text), Type = MessageType.SelfText, Received = DateTime.Now, Sender = IPAddress.Loopback }
            );
            this.ScrollViewer.ScrollToEnd();
            this.TextBox.Text = null;
        }

        private void Communicator_OnKeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.C) &&
                ((Keyboard.Modifiers & (ModifierKeys.Control | ModifierKeys.Shift)) ==
                 (ModifierKeys.Control | ModifierKeys.Shift)))
                this.ConnectClick(this, null);
        }

        private void Communicator_OnClosing(object sender, CancelEventArgs e)
        {
            Connection.Connection.RemoveUPnP();
        }
    }
}