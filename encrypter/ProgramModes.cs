﻿using System.ComponentModel;

namespace encrypter
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum ProgramMode
    {
        [Description("Szyfr Cezara - Tekst")] CaesarText,
        [Description("Szyfr Cezara - Obraz")] CaesarImage
    }
}