﻿using System;
using System.Linq;
using System.Net;
using System.Numerics;
using System.Text;
using System.Threading;
using encrypter.Cipher;

namespace encrypter.Connection
{
    class Protocol
    {
        public const ushort PROT_OK = 0x16;
        public const ushort PROT_ERROR = 0x01;
        public const ushort PROT_KEY_REQ = 0x11;
        public const ushort PROT_KEY_PUB = 0x75;
        public const ushort PROT_KEY_SYM = 0x26;
        public const ushort PROT_HELLO = 0xFF;
        public const ushort PROT_ACCEPT = 0xFE;
        public const ushort PROT_CONTINUE = 0x19;
        public const ushort PROT_CHECKSUM = 0x55;
        public const ushort PROT_MSG_TXT = 0x22;
        public const ushort PROT_MSG_IMG = 0x33;
        public const ushort PROT_MSG_FILE = 0x44;

        private Connection _connection;
        private RSACipher _rsa;
        private AESCipher _aes;

        private BigInteger[] _rsaPublicKey;
        private byte[] _aesSymKey;

        //private ushort? _flagSet;
        private byte[] _buffer;
        private ushort _length;
        private ushort _msgPart;

        public bool IsConnected { get; private set; }

        public IPAddress ConnectedIP
        {
            get
            {
                return this._connection.ConnectedIP;
            }
        }

        private ManualResetEvent _publicKeyEvent;
        private ManualResetEvent _dataReceivedEvent;

        public event Connection.ConnectedHandler Connected;
        public event Connection.MessageReceivedHandler DataReceived;
        public event Connection.DisconnectedHandler Disconnected;

        public Protocol()
        {
            this.IsConnected = false;
            this._connection = new Connection();
            this._connection.Connected += (isClient) =>
            {
                if(!isClient) return;

                this._connection.Send(new[] { (byte)PROT_HELLO });
                this._dataReceivedEvent.WaitOne();
                this._dataReceivedEvent.Reset();
                this.NegotiateKeys();
            };

            this._connection.Disconnected += () =>
            {
                if (this.Disconnected != null) this.Disconnected();
            };

            this._connection.MessageReceived += this.ConnectionOnMessageReceived;

            this._dataReceivedEvent = new ManualResetEvent(false);

            this._rsa = new RSACipher();
            this._aes = new AESCipher();
        }

        public void Connect(IPAddress ip)
        {
            this._connection.Connect(ip);
        }

        public void Send(byte[] data)
        {
            if (!this._connection.IsConnected) throw new Exception("Not connected");

            if (data.Length <= 251)
            {
                var length = BitConverter.GetBytes(data.Length);

                var msg =
                    (new[] { (byte)PROT_MSG_TXT }).Concat(length).Concat(this._aes.Encrypt(data, this._aesSymKey));
                this._connection.Send(msg.ToArray());
            }
            else
            {
                var encrypted = this._aes.Encrypt(data, this._aesSymKey);
                var toSend = encrypted.ToList();
                toSend.Add((byte)(encrypted.Length - data.Length));
                var parts = toSend.Select((x, i) => new { Index = i, Value = x })
                            .GroupBy(x => x.Index / 251)
                            .Select(x => x.Select(v => v.Value).ToList())
                            .ToList();

                var msg =
                    (new[] { (byte)PROT_MSG_TXT }).Concat(BitConverter.GetBytes(toSend.Count)).Concat(parts[0]);
                this._connection.Send(msg.ToArray());
                this._dataReceivedEvent.WaitOne();

                foreach (var part in parts.Skip(1))
                {
                    msg = (new[] { (byte)PROT_CONTINUE }).Concat(BitConverter.GetBytes(part.Count)).Concat(part);
                    this._connection.Send(msg.ToArray());
                    this._dataReceivedEvent.WaitOne();
                }
            }
        }

        public void SendFile(byte[] data, MessageType type)
        {
            if(type != MessageType.Image) return;
        }

        private void ConnectionOnMessageReceived(byte[] data)
        {
            if(data == null) return;
            var flag = (ushort)data[0];

            switch (flag)
            {
                case PROT_HELLO:
                    Console.WriteLine(" {0} RECEIVED: HELLO", DateTime.Now);
                    this._connection.Send(new[] { (byte)PROT_ACCEPT });
                    this.IsConnected = true;
                    if (this.Connected != null) this.Connected(false);
                    break;
                case PROT_ACCEPT:
                    Console.WriteLine(" {0} RECEIVED: ACCEPT", DateTime.Now);
                    this._dataReceivedEvent.Set();
                    if (this.Connected != null) this.Connected(true);
                    this.IsConnected = true;
                    break;
                case PROT_KEY_PUB:
                    Console.WriteLine(" {0} RECEIVED: PUBLIC KEY", DateTime.Now);
                    this._rsaPublicKey = new BigInteger[2];
                    this._rsaPublicKey[0] = new BigInteger(data.Skip(1).Take(32).ToArray());
                    this._rsaPublicKey[1] = new BigInteger(data.Skip(33).Take(32).ToArray());
                    this._connection.Send(new []{ (byte)PROT_OK });
                    this._publicKeyEvent.Set();
                    break;
                case PROT_KEY_REQ:
                    Console.WriteLine(" {0} RECEIVED: KEY REQUEST", DateTime.Now);
                    var toSend = new byte[128];
                    toSend[0] = (byte) PROT_KEY_PUB;
                    this._rsa.PublicKey[0].ToByteArray().CopyTo(toSend, 1);
                    this._rsa.PublicKey[1].ToByteArray().CopyTo(toSend, 33);
                    this._connection.Send(toSend);
                    break;
                case PROT_KEY_SYM:
                    Console.WriteLine(" {0} RECEIVED: SYMMETRIC KEY", DateTime.Now);
                    var decryptedKey = this._rsa.Decrypt(data.Skip(1).Take(32).ToArray());
                    this._aesSymKey = decryptedKey.Take(16).ToArray();
                    this._connection.Send(new[] { (byte)PROT_OK });
                    break;
                case PROT_MSG_TXT:
                    Console.WriteLine(" {0} RECEIVED: MESSAGE!", DateTime.Now);
                    if (BitConverter.ToInt32(data.Skip(1).Take(4).ToArray(), 0) > 251)
                    {
                        this._buffer = new byte[BitConverter.ToInt32(data.Skip(1).Take(4).ToArray(), 0)];
                        data.Skip(5).ToArray().CopyTo(this._buffer, 0);
                        this._connection.Send(new[] { (byte)PROT_OK });
                        this._msgPart++;
                    }
                    else
                    {
                        this.DecryptMessage(data.Skip(5).ToArray(), BitConverter.ToInt32(data.Skip(1).Take(4).ToArray(), 0));
                    }

                    break;
                case PROT_OK:
                    this._dataReceivedEvent.Set();
                    this._dataReceivedEvent.Reset();
                    break;
                case PROT_CONTINUE:
                    if(this._buffer == null) throw new Exception("Internal program error.");

                    data.Skip(5).Take(BitConverter.ToInt32(data.Skip(1).Take(4).ToArray(), 0)).ToArray().CopyTo(this._buffer, (this._msgPart * 251));
                    this._msgPart++;
                    this._connection.Send(new[] { (byte)PROT_OK });

                    if (this._msgPart*251 >= this._buffer.Length)
                    {
                        this._connection.Send(new[] {(byte) PROT_OK});
                        this._msgPart = 0;
                        this._length = this._buffer.Last();
                        this.DecryptMessage(this._buffer, (this._buffer.Length - 1) - this._length);
                    }

                    break;
            }
        }

        private void DecryptMessage(byte[] data, int dataLength)
        {
            Console.WriteLine(" {0} DECRYPTION: RECEIVED BYTE DATA", DateTime.Now);
            var decrypt = this._aes.Decrypt(data, this._aesSymKey);
            Console.WriteLine(" {0} DECRYPTION: DONE", DateTime.Now);

            if (this.DataReceived != null) this.DataReceived(decrypt.Take(dataLength).ToArray());
        }

        private void NegotiateKeys()
        {
            this._publicKeyEvent = new ManualResetEvent(false);
            this._connection.Send(new[] { (byte)PROT_KEY_REQ });
            this._publicKeyEvent.WaitOne();
            //this._publicKeyEvent.Reset();
            var toSend = new byte[256];
            toSend[0] = (byte)PROT_KEY_SYM;
            this._rsa.Encrypt(this._aes.Key, this._rsaPublicKey).CopyTo(toSend, 1);
            this._connection.Send(toSend);
            this._aesSymKey = this._aes.Key;
            this._dataReceivedEvent.WaitOne();
            //this._dataReceivedEvent.Reset();
        }
    }
}
