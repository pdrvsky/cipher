﻿using System;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Windows;

namespace encrypter.Connection
{
    internal class Connection
    {
        public delegate void ConnectedHandler(bool isClient);

        public delegate void MessageReceivedHandler(byte[] data);

        public delegate void DisconnectedHandler();

        private byte[] _buffer;
        private Socket _connectedSocket;

        private byte[] _message;

        private Socket _serverSocket;

        public Connection()
        {
            this._serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            this._serverSocket.Bind(new IPEndPoint(IPAddress.Any, 75));
            this._serverSocket.Listen(1);
            this._serverSocket.BeginAccept(this.AcceptCallback, this._serverSocket);

            var ipadd = GetPhysicalIPAdress();

            var upnpnat = new NATUPNPLib.UPnPNATClass();
            NATUPNPLib.IStaticPortMappingCollection mappings = upnpnat.StaticPortMappingCollection;
            mappings.Add(75, "TCP", 75, ipadd, true, "Encrypter");
        }

        public bool IsConnected { get; private set; }
        public IPAddress ConnectedIP { get; private set; }
        public event MessageReceivedHandler MessageReceived;
        public event ConnectedHandler Connected;
        public event DisconnectedHandler Disconnected;

        public void Connect(IPAddress ip)
        {
            this._serverSocket.Close();
            this._serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            this._serverSocket.BeginConnect(ip, 75, this.ConnectedCallback, null);

        }

        public void Send(byte[] msg)
        {
            this._connectedSocket.BeginSend(msg, 0, msg.Length, SocketFlags.None, this.SendCallback, null);
        }

        private void ConnectedCallback(IAsyncResult ar)
        {
            this._serverSocket.EndConnect(ar);
            this._connectedSocket = this._serverSocket;

            this._buffer = new byte[256];
            this._serverSocket.BeginReceive(this._buffer, 0, this._buffer.Length, SocketFlags.None,
                this.ReceiveCallback, null);

            this.ConnectedIP = ((IPEndPoint)this._connectedSocket.RemoteEndPoint).Address;
            this.IsConnected = true;
            if (this.Connected != null) this.Connected(true);
        }

        private void SendCallback(IAsyncResult ar)
        {
            this._connectedSocket.EndSend(ar);
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            var server = ar.AsyncState as Socket;
            if (server == null) return;

            try
            {
                this._connectedSocket = server.EndAccept(ar);
            }
            catch (ObjectDisposedException) { return; }

            this._buffer = new byte[256];
            this.IsConnected = true;

            this._connectedSocket.BeginReceive(this._buffer, 0, this._buffer.Length, SocketFlags.None,
                this.ReceiveCallback, null);

            this.ConnectedIP = ((IPEndPoint)this._connectedSocket.RemoteEndPoint).Address;
            if (this.Connected != null) this.Connected(false);
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                var msgLength = this._connectedSocket.EndReceive(ar);
                if (msgLength <= 0) return;

                // odczytanie danych
                this._connectedSocket.BeginReceive(this._buffer, 0, this._buffer.Length, SocketFlags.None,
                    this.ReceiveCallback, null);

                // przygotowanie wiadomosci
                this._message = this._buffer.Take(msgLength).ToArray();

                if (this.MessageReceived != null)
                    this.MessageReceived(this._message);
            }
            catch (SocketException)
            {
                Console.WriteLine("{0}: SOCKET EXCEPTION!", DateTime.Now);
                this.IsConnected = false;
                if (this.Disconnected != null) this.Disconnected();
            }
        }

        public static string GetPhysicalIPAdress()
        {
            foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                GatewayIPAddressInformation addr = ni.GetIPProperties().GatewayAddresses.FirstOrDefault();

                if (addr == null || addr.Address.ToString().Equals("0.0.0.0")) continue;

                if (ni.NetworkInterfaceType != NetworkInterfaceType.Wireless80211 &&
                    ni.NetworkInterfaceType != NetworkInterfaceType.Ethernet) continue;

                foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses)
                {
                    if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
                    {
                        return ip.Address.ToString();
                    }
                }
            }

            return string.Empty;
        }

        public static void RemoveUPnP()
        {
            var upnpnat = new NATUPNPLib.UPnPNATClass();
            NATUPNPLib.IStaticPortMappingCollection mappings = upnpnat.StaticPortMappingCollection;
            mappings.Remove(75, "TCP");
        }
    }
}