﻿using System;
using System.Linq;
using System.Security.Cryptography;

namespace encrypter.Cipher
{
    internal class AESCipher : ICipher, IDisposable
    {
        public byte[] Key { get; private set; }

        public AESCipher()
        {
            using (var rnd = new RNGCryptoServiceProvider())
            {
                this.Key = new byte[16];
                rnd.GetNonZeroBytes(this.Key);
            }
        }

        public byte[] Encrypt(byte[] input)
        {
            var blocks = AESExtensions.SplitToBlocks(input);
            var states = new byte[blocks.GetLength(0)][,];
            var roundKey = AESExtensions.KeySchedule(this.Key);
            var roundKeyStates = new byte[11][,];

            for (var i = 0; i < blocks.Length; i++)
                states[i] = AESExtensions.State(blocks[i]);

            for (var i = 0; i < roundKeyStates.GetLength(0); i++)
            {
                var key = roundKey.Where((x, index) => (index >= i*16) && (index < i*16 + 16)).ToArray();
                roundKeyStates[i] = AESExtensions.State(key);
            }

            for (var el = 0; el < states.Length; el++)
            {
                states[el] = AESExtensions.AddRoundKey(roundKeyStates[0], states[el]);

                for (var i = 1; i < 10; i++)
                {
                    states[el] = AESExtensions.SubByte(states[el]);
                    states[el] = AESExtensions.ShiftRows(states[el]);
                    states[el] = AESExtensions.MixColumns(states[el]);
                    states[el] = AESExtensions.AddRoundKey(roundKeyStates[i], states[el]);
                }

                states[el] = AESExtensions.SubByte(states[el]);
                states[el] = AESExtensions.ShiftRows(states[el]);
                states[el] = AESExtensions.AddRoundKey(roundKeyStates.Last(), states[el]);
            }

            var cipher = new byte[blocks.Length*16];

            for (var i = 0; i < states.Length; i++)
            {
                var unstate = AESExtensions.UnState(states[i]);
                unstate.CopyTo(cipher, i*16);
            }

            return cipher;
        }

        public byte[] Encrypt(byte[] input, byte[] extKey)
        {
            var blocks = AESExtensions.SplitToBlocks(input);
            var states = new byte[blocks.GetLength(0)][,];
            var roundKey = AESExtensions.KeySchedule(extKey);
            var roundKeyStates = new byte[11][,];

            for (var i = 0; i < blocks.Length; i++)
                states[i] = AESExtensions.State(blocks[i]);

            for (var i = 0; i < roundKeyStates.GetLength(0); i++)
            {
                var key = roundKey.Where((x, index) => (index >= i * 16) && (index < i * 16 + 16)).ToArray();
                roundKeyStates[i] = AESExtensions.State(key);
            }

            for (var el = 0; el < states.Length; el++)
            {
                states[el] = AESExtensions.AddRoundKey(roundKeyStates[0], states[el]);

                for (var i = 1; i < 10; i++)
                {
                    states[el] = AESExtensions.SubByte(states[el]);
                    states[el] = AESExtensions.ShiftRows(states[el]);
                    states[el] = AESExtensions.MixColumns(states[el]);
                    states[el] = AESExtensions.AddRoundKey(roundKeyStates[i], states[el]);
                }

                states[el] = AESExtensions.SubByte(states[el]);
                states[el] = AESExtensions.ShiftRows(states[el]);
                states[el] = AESExtensions.AddRoundKey(roundKeyStates.Last(), states[el]);
            }

            var cipher = new byte[blocks.Length * 16];

            for (var i = 0; i < states.Length; i++)
            {
                var unstate = AESExtensions.UnState(states[i]);
                unstate.CopyTo(cipher, i * 16);
            }

            return cipher;
        }

        public byte[] Decrypt(byte[] input)
        {
            var blocks = AESExtensions.SplitToBlocks(input);
            var states = new byte[blocks.GetLength(0)][,];
            var roundKey = AESExtensions.KeySchedule(this.Key);
            var roundKeyStates = new byte[11][,];

            for (var i = 0; i < blocks.Length; i++)
                states[i] = AESExtensions.State(blocks[i]);

            for (var i = 0; i < roundKeyStates.GetLength(0); i++)
            {
                var key = roundKey.Where((x, index) => (index >= i*16) && (index < i*16 + 16)).ToArray();
                roundKeyStates[i] = AESExtensions.State(key);
            }


            for (var el = 0; el < states.Length; el++)
            {
                states[el] = AESExtensions.AddRoundKey(roundKeyStates[10], states[el]);
                states[el] = AESExtensions.InverseShiftRows(states[el]);
                states[el] = AESExtensions.InverseSubByte(states[el]);

                for (var i = 9; i >= 1; i--)
                {
                    states[el] = AESExtensions.AddRoundKey(roundKeyStates[i], states[el]);
                    states[el] = AESExtensions.InverseMixColumns(states[el]);
                    states[el] = AESExtensions.InverseShiftRows(states[el]);
                    states[el] = AESExtensions.InverseSubByte(states[el]);
                }

                states[el] = AESExtensions.AddRoundKey(roundKeyStates[0], states[el]);
            }


            var cipher = new byte[blocks.Length*16];

            for (var i = 0; i < states.Length; i++)
            {
                var unstate = AESExtensions.UnState(states[i]);
                unstate.CopyTo(cipher, i*16);
            }

            return cipher;
        }

        public byte[] Decrypt(byte[] input, byte[] extKey)
        {
            var blocks = AESExtensions.SplitToBlocks(input);
            var states = new byte[blocks.GetLength(0)][,];
            var roundKey = AESExtensions.KeySchedule(extKey);
            var roundKeyStates = new byte[11][,];

            for (var i = 0; i < blocks.Length; i++)
                states[i] = AESExtensions.State(blocks[i]);

            for (var i = 0; i < roundKeyStates.GetLength(0); i++)
            {
                var key = roundKey.Where((x, index) => (index >= i * 16) && (index < i * 16 + 16)).ToArray();
                roundKeyStates[i] = AESExtensions.State(key);
            }


            for (var el = 0; el < states.Length; el++)
            {
                states[el] = AESExtensions.AddRoundKey(roundKeyStates[10], states[el]);
                states[el] = AESExtensions.InverseShiftRows(states[el]);
                states[el] = AESExtensions.InverseSubByte(states[el]);

                for (var i = 9; i >= 1; i--)
                {
                    states[el] = AESExtensions.AddRoundKey(roundKeyStates[i], states[el]);
                    states[el] = AESExtensions.InverseMixColumns(states[el]);
                    states[el] = AESExtensions.InverseShiftRows(states[el]);
                    states[el] = AESExtensions.InverseSubByte(states[el]);
                }

                states[el] = AESExtensions.AddRoundKey(roundKeyStates[0], states[el]);
            }


            var cipher = new byte[blocks.Length * 16];

            for (var i = 0; i < states.Length; i++)
            {
                var unstate = AESExtensions.UnState(states[i]);
                unstate.CopyTo(cipher, i * 16);
            }

            return cipher;
        }

        public void Dispose()
        {
            this.Key = null;
        }
    }
}