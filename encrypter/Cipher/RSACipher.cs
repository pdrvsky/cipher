﻿using System;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;

namespace encrypter.Cipher
{
    internal class RSACipher : ICipher, IDisposable
    {
        private BigInteger[] _privateKey;
        private BigInteger[] _publicKey;

        public BigInteger[] PublicKey { get { return this._publicKey; } }

        public RSACipher()
        {
            this._privateKey = new BigInteger[2];
            this._publicKey = new BigInteger[2];

            this.GenerateRSAKeys();
        }

        public byte[] Encrypt(byte[] input)
        {
            if (this._publicKey == null) throw new CryptographicException("Public key not set.");

            return BigInteger.ModPow(new BigInteger(input), this._publicKey[0], this._publicKey[1]).ToByteArray();
        }

        public byte[] Encrypt(byte[] input, BigInteger[] key)
        {
            if (key == null) throw new CryptographicException("Public key not set.");

            return BigInteger.ModPow(new BigInteger(input), key[0], key[1]).ToByteArray();
        }

        public byte[] Decrypt(byte[] input)
        {
            if (this._privateKey == null) throw new CryptographicException("Private key not set.");

            return BigInteger.ModPow(new BigInteger(input), this._privateKey[0], this._privateKey[1]).ToByteArray();
        }

        private void GenerateRSAKeys()
        {
            byte[] pB = new byte[16], qB = new byte[16];
            using (RandomNumberGenerator rng = RandomNumberGenerator.Create())
            {
                do
                {
                    rng.GetNonZeroBytes(pB);
                } while (!this.IsPrimeMillerRabin(pB));
                do
                {
                    rng.GetNonZeroBytes(qB);
                } while (!this.IsPrimeMillerRabin(qB));

                BigInteger e, d;
                BigInteger p = BigInteger.Abs(new BigInteger(pB));
                BigInteger q = BigInteger.Abs(new BigInteger(qB));

                BigInteger n = p*q;
                BigInteger phi = (p - 1)*(q - 1);

                do
                {
                    var eBuff = new byte[phi.ToByteArray().LongLength];
                    rng.GetBytes(eBuff);
                    e = new BigInteger(eBuff);
                    d = this.ExtendedEuclidean(e, phi);
                } while ((1 > e) || (phi < e) || !this.Euclidean(e, phi) || (d < 0));

                this._privateKey[0] = d;
                this._privateKey[1] = n;

                this._publicKey[0] = e;
                this._publicKey[1] = n;
            }
        }

        private bool IsPrimeMillerRabin(byte[] input)
        {
            var number = new BigInteger(input);
            number = BigInteger.Abs(number);
            BigInteger d = number - 1;
            var maxPowIndex = 0;

            if (number.IsEven) return false;

            while (d%2 == 0)
            {
                d >>= 1;
                maxPowIndex += 1;
            }

            RandomNumberGenerator rng = RandomNumberGenerator.Create();
            var bytes = new byte[input.LongLength];

            for (var i = 63; i >= 0; i--)
            {
                BigInteger a;
                do
                {
                    rng.GetBytes(bytes);
                    a = new BigInteger(bytes);
                } while ((a < 2) || (a >= number - 2));

                BigInteger x = BigInteger.ModPow(a, d, number);

                if ((x == 1) || (x == number - 1)) continue;

                for (var j = 0; j < maxPowIndex; j++)
                {
                    x = BigInteger.ModPow(x, 2, number);
                    if (x == 1) return false;
                    if (x == number - 1) break;
                }

                if (x != number - 1) return false;
            }

            return true;
        }

        private BigInteger ExtendedEuclidean(BigInteger a, BigInteger b)
        {
            BigInteger u1 = 1;
            BigInteger u3 = a;
            BigInteger v1 = 0;
            BigInteger v3 = b;

            while (v3 > 0)
            {
                BigInteger q0 = u3/v3;
                BigInteger q1 = u3%v3;

                BigInteger tmp = v1*q0;
                BigInteger tn = u1 - tmp;
                u1 = v1;
                v1 = tn;

                u3 = v3;
                v3 = q1;
            }

            return u1;
        }

        private bool Euclidean(BigInteger a, BigInteger b)
        {
            while (b != 0)
            {
                BigInteger t = b;
                b = a%b;
                a = t;
            }

            return a == 1;
        }

        //private bool CoPrime(BigInteger u, BigInteger v)
        //{
        //    if (((u | v) & 1) == 0) return false;

        //    while ((u & 1) == 0) u >>= 1;
        //    if (u == 1) return true;

        //    do
        //    {
        //        while ((v & 1) == 0) v >>= 1;
        //        if (v == 1) return true;

        //        if (u > v)
        //        {
        //            BigInteger t = v;
        //            v = u;
        //            u = t;
        //        }
        //        v -= u;
        //    } while (v != 0);

        //    return false;
        //}

        public void Dispose()
        {
            this._privateKey = null;
            this._publicKey = null;
        }
    }
}