﻿namespace encrypter.Cipher
{
    public class CaesarCipher : ICipher
    {
        private readonly byte _shift;

        public CaesarCipher(byte shift)
        {
            this._shift = shift;
        }

        public byte[] Encrypt(byte[] input)
        {
            var encodedData = new byte[input.Length];
            for (var i = 0; i < input.Length; i++)
                if (input[i] + this._shift <= byte.MaxValue) encodedData[i] = (byte) (input[i] + this._shift);
                else encodedData[i] = (byte) (input[i] + this._shift - byte.MaxValue);

            return encodedData;
        }

        public byte[] Decrypt(byte[] input)
        {
            var encodedData = new byte[input.Length];
            for (var i = 0; i < input.Length; i++)
                if (input[i] - this._shift >= byte.MinValue) encodedData[i] = (byte) (input[i] - this._shift);
                else encodedData[i] = (byte) (byte.MaxValue + (input[i] - this._shift));

            return encodedData;
        }
    }
}