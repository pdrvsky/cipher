﻿namespace encrypter.Cipher
{
    internal interface ICipher
    {
        byte[] Encrypt(byte[] input);
        byte[] Decrypt(byte[] input);
    }
}