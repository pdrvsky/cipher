﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace encrypter
{
    /// <summary>
    /// Interaction logic for Communicator.xaml
    /// </summary>
    public partial class Communicator
    {
        private TcpClient _clientSocket;
        private TcpListener _serverSocket;

        public Communicator()
        {
            this.InitializeComponent();
            this._serverSocket = new TcpListener(IPAddress.Any, 75); this._serverSocket.Start();
            this._serverSocket.BeginAcceptTcpClient(this.AcceptClient, this._serverSocket);
            this._clientSocket = new TcpClient();
        }

        private void AcceptClient(IAsyncResult ar)
        {
            byte[] buffer = new byte[1024];
            var socket = ar.AsyncState as TcpListener;
        }

        private void ConnectClick(object sender, RoutedEventArgs e)
        {
            var connect = new Connect();
            if (connect.ShowDialog() != true) return;

            this._clientSocket.BeginConnect(connect.IP, 75, this.RequestCallback, this._clientSocket);
            this.Title = "Communicator - connecting...";
        }

        private void RequestCallback(IAsyncResult ar)
        {
            var tcp = ar.AsyncState as TcpClient;
            if (tcp == null) return;

            try
            {
                tcp.EndConnect(ar);
            }
            catch (Exception)
            {
                this.Dispatcher.Invoke(() =>
                {
                    this.Title = "Communicator";
                });

                MessageBox.Show(
                        "Failed to connect. Check your internet connection and try again.", "Connect error", MessageBoxButton.OK,
                        MessageBoxImage.Error);

                return;
            }

            this.Dispatcher.Invoke(() =>
            {
                this.Title = "Communicator - Connected to: " + tcp.Client.RemoteEndPoint;
            });
        }
    }
}
