﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace encrypter
{
    /// <summary>
    /// Interaction logic for Connect.xaml
    /// </summary>
    public partial class Connect
    {
        public IPAddress IP { get; private set; }

        public Connect()
        {
            this.InitializeComponent();
            this.IPBox.Focus();
        }


        private void ConnectButton_OnClick(object sender, RoutedEventArgs e)
        {
            var ip = Dns.GetHostAddresses(this.IPBox.Text);

            if (ip.Length == 0)
                MessageBox.Show("Not valid address!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            else
            {
                this.IP = ip.Last();
                this.DialogResult = true;
                this.Close();
            }
        }

        private void IPBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) this.ConnectButton_OnClick(sender, null);
        }
    }
}
